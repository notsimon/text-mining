#! /usr/bin/env python

import sys
import os
import io

# Searcher
class Searcher:
	def __init__ (self):
		self.didToUrl = list()
		self.wordToDids = dict()

	def load(self, index_filename):
		index = open(index_filename, 'r')
		
		def remove_newline(line):
			return line[0:len(line) - 1]

		lines = map(remove_newline, index.readlines())
		i = 0
		while lines[i] != "WORDTODIDS":
			self.didToUrl.append(lines[i])
			i += 1
		for line in lines[i + 1:len(lines)]:
			line_array = line.split()

			def str_to_int(s):
				return int(s)

			dids = map(str_to_int, line_array[1:len(line_array)])
			dids.sort()
			self.wordToDids[line_array[0]] = dids

	def search(self, search):
		results = list()
		parsing_stack = search.split()
		parsing_stack.reverse()
		for did in self.parse_search(parsing_stack):
			results.append(self.didToUrl[did])
		return results

	# search :=
	# | <term>
	# | <term> OR <search>
	def parse_search(self, parsing_stack):
		result = self.parse_term(parsing_stack)
		if len(parsing_stack) > 0 and parsing_stack[len(parsing_stack) - 1] == "OR":
			parsing_stack.pop()
			return self.search_or(result, self.parse_search(parsing_stack))
		return result		

	# term :=
	# | <factor>
	# | <factor> AND <term>
	def parse_term(self, parsing_stack):
		result = self.parse_factor(parsing_stack)
		if len(parsing_stack) > 0 and parsing_stack[len(parsing_stack) - 1] == "AND":
			parsing_stack.pop()
			return self.search_and(result, self.parse_term(parsing_stack))
		return result

	# factor :=
	# | word
	# | NOT factor
	# | ( <search> )
	def parse_factor(self, parsing_stack):
		if len(parsing_stack) > 0 and parsing_stack[len(parsing_stack) - 1] == "(":
			parsing_stack.pop()
			result = self.parse_search(parsing_stack)
			parsing_stack.pop()
		elif len(parsing_stack) > 0 and parsing_stack[len(parsing_stack) - 1] == "NOT":
			parsing_stack.pop()
			result = self.search_not(self.parse_search(parsing_stack))
		else:
			return self.search_word(parsing_stack.pop())
		return result

	def search_word(self, word):
		if word in self.wordToDids:
			return self.wordToDids[word]
		return list()

	def search_not(self, dids):
		d = 0
		a = 0
		result = list()
		while d < len(dids) and a < len(self.didToUrl):
			if dids[d] == a:
				d += 1
				a += 1
			elif dids[d] <= a:
				d += 1
			else:
				result.append(a)
				a += 1
		if d == len(dids):
			result += range(a, len(self.didToUrl))

		return result

	def search_and(self, ldids, rdids):
		l = 0
		r = 0
		result = list()
		while l < len(ldids) and r < len(rdids):
			if ldids[l] == rdids[r]:
				result.append(rdids[r])
				l += 1
				r += 1
			elif ldids[l] < rdids[r]:
				l += 1
			else:
				r += 1
		return result


	def search_or(self, ldids, rdids):
		l = 0
		r = 0
		result = list()
		while l < len(ldids) and r < len(rdids):
			if ldids[l] == rdids[r]:
				result.append(rdids[r])
				l += 1
				r += 1
			elif ldids[l] < rdids[r]:
				result.append(ldids[l])
				l += 1
			else:
				result.append(rdids[r])
				r += 1
		if l == len(ldids):
			result += rdids[r:len(rdids)]
		else:
			result += ldids[l:len(ldids)]
		return result


# Main

if len(sys.argv) > 1:
	s = Searcher()
	s.load(sys.argv[1])
else:
	print('usage:', sys.argv[0], '[index]', '[search]')
if len(sys.argv) > 2:
	try:
		results = s.search(sys.argv[2])
	except Exception:
		print "Error: Invalid search."
		sys.exit(1)
	if len(results) > 0:
		print "Documents matching '" + sys.argv[2] + "':"
		for r in results:
			print r
	else:
		print "No document matching '" + sys.argv[2] + "' found."
