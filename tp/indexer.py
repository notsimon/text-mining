#! /usr/bin/env python

import unicodedata
import sys
import os
import io
#import mimetypes
#import re

# Connecteur

class Document:
	def __init__ (self, url, text):
		self.text = text
		self.url = url

def fetch (path, recursive = True):
	documents = []

	for fp in os.listdir(path):
		fp = os.path.join(path, fp)
		if os.path.isdir(fp):
			if recursive:
				documents += fetch(fp, recursive)
		else:
			#(ft, enc) = mimetypes.guess_type(fp)
			#if ft != None and re.match('text/.*', ft):
			f = open(fp, 'r')
			text = f.read()
			documents.append(Document(fp, text))

	return documents

# Analyseur

class TokenizedDocument:
	def __init__ (self, url, words):
		self.url = url
		self.words = words

def analyze (documents, processors):
	tokenized = []

	for document in documents:
		words = document.text.split()

		def process_word (word):
			for p in processors:
				word = p.process(word)
			return word

		words = list(map(process_word, words))
		tokenized.append(TokenizedDocument(document.url, words))

	return tokenized

class TextProcessor:
	def process (word):
		pass

class Normalizer (TextProcessor):
	@staticmethod
	def process (word):
		"""Removes accents and lower the string"""
		word = word.lower()
		if isinstance(word,str):
			word = unicode(word,'utf8', 'ignore')
		word = unicodedata.normalize('NFD', word)
		return word.encode('ascii','ignore')

# Indexeur

class Posting:
	def __init__ (self, word):
		self.word = word

	@staticmethod
	def index(tok_docs):
		wordToPostingIndex = dict()
		postings = list()
		i = 0
		for doc in tok_docs:
			for w in doc.words:
				if w in wordToPostingIndex:
					postings[wordToPostingIndex[w]].urls += [doc.url]
				else:
					wordToPostingIndex[w] = i
					postings.append(Posting(w))
					postings[i].urls = list([doc.url])
					i += 1
		return postings

class Index:
	def __init__ (self):
		self.didToUrl = list()
		self.wordToDids = dict()
		self.urlToDid = dict()
		self.ndoc = 0

	def build(self, postings):
		for p in postings:
			if p.word not in self.wordToDids:
				self.wordToDids[p.word] = list()
			for url in p.urls:
				self.wordToDids[p.word].append(self.get_document_did(url))
	
	def get_document_did(self, url):
		if url in self.urlToDid:
			return self.urlToDid[url]
		else:
			self.urlToDid[url] = self.ndoc
			self.didToUrl.append(url)
			self.ndoc += 1
			return self.urlToDid[url]

        def save(self, output_filename):
                out = open(output_filename, 'w')
                for url in self.didToUrl:
                        out.write(url + '\n')
                out.write("WORDTODIDS\n")
                for item in self.wordToDids.items():
                        out.write(item[0])
                        for did in item[1]:
                                out.write(" " + str(did))
                        out.write('\n')
# Main

if len(sys.argv) > 2:
	documents = fetch(sys.argv[1])
	tokenized_documents = analyze(documents, [Normalizer])
	print(len(tokenized_documents))
	index = Index()
	index.build(Posting.index(tokenized_documents))
        index.save(sys.argv[2])
else:
	print('usage:', sys.argv[0], '[directory]', '[output file name]')
