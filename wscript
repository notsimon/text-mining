#! /usr/bin/env python

import shutil

APPNAME = 'TextMining-guillo_b-keo_r-bethmo_l'
VERSION = '1.0'

top = '.'
out = 'build'

def options(ctx):
	ctx.load('compiler_cxx unittest_gtest test')

	ctx.add_option(
		'-m', '--mode',
		action			= 'store',
		default			= 'release',
		help			= 'build mode, release or debug'
	)

	ctx.add_option(
		'-i', '--include',
		action			= 'store',
		metavar			= 'PATH',
		help			= 'add PATH to the include search paths'
	)

	ctx.add_option(
		'-l', '--library',
		action			= 'store',
		metavar 		= 'PATH',
		help 			= 'add PATH to the static library search paths'
	)

def configure(ctx):
	ctx.load('compiler_cxx unittest_gtest test')

	# add user defined paths
	if ctx.options.include is not None:
		ctx.env.append_unique('INCLUDES', os.path.realpath(ctx.options.include))

	if ctx.options.library is not None:
		ctx.env.append_unique('STLIBPATH', os.path.realpath(ctx.options.library))

def copy(ctx):
	shutil.copy2(out + "/TextMiningCompiler", top + "/TextMiningCompiler")
	shutil.copy2(out + "/TextMiningApp", top + "/TextMiningApp")

def build(ctx):
	cxxflags = {
		'release': ['-O4', '-w', '-DNDEBUG'],
		'debug': ['-O0', '-W', '-Wall', '-g', '-ggdb']
	}
	ctx.env.append_unique('CXXFLAGS', cxxflags[ctx.options.mode])

	ctx.env.append_unique('INCLUDES', 'source');

	ctx.stlib(
		source		= ctx.path.ant_glob(['source/common/*.cc', 'source/compiler/*.cc']),
		target		= 'Trie',

		cxxflags	= '-std=c++11'
	)

	ctx.program(
		target		= 'TextMiningApp',
		source		= ctx.path.ant_glob('source/app/*.cc'),
		use			= 'Trie',

		cxxflags	= '-std=c++11',
	)

	ctx.program(
		target		= 'TextMiningCompiler',
		source		= ctx.path.ant_glob('source/compiler/*.cc'),
		use			= 'Trie',

		cxxflags	= '-std=c++11',
	)

	ctx.add_post_fun(copy)

	# unit tests

	ctx.program(
		target		= 'test',
		features	= 'cxx cxxprogram gtest',
		cxxflags	= '-std=c++11',

		source		= ctx.path.ant_glob('tests/*.cc')
	)

def dist(ctx):
	ctx.files = ctx.path.ant_glob([
					'wscript', 'waf', 'README', 'AUTHORS', 'doxyfile', 'unittest_gtest.py', \
					'source/**.cc', 'source/**.hxx', 'source/**.hh', \
					'source/common/**.cc', 'source/common/**.hxx', 'source/common/**.hh', \
					'source/app/**.cc', 'source/app/**.hxx', 'source/app/**.hh', \
					'source/compiler/**.cc', 'source/compiler/**.hxx', 'source/compiler/**.hh', \
					'tests/**.cc', 'tp/*.py'
				])
