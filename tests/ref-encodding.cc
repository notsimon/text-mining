#include <iostream>

#include <gtest/gtest.h>
#include "common/ref-encoded-string.hh"

char decode(std::string str, int i) {
	RefEncodedString s(str);

	return s.dec(i);
}

char has_next(std::string str, int i) {
	RefEncodedString s(str);

	return s.has_next(i);
}

bool concat(std::string a, std::string b) {
	RefEncodedString s(b);

	std::string ab(a + b);
	a += b;

	return ab.compare(a) == 0;
}

TEST(RefEncoding, Decode) {
	ASSERT_EQ(decode("abc", 0), 'a');
	ASSERT_EQ(decode("de", 1), 'e');
	ASSERT_EQ(decode("_", 0), '_');
	ASSERT_EQ(decode("+-)", 0), '+');
	ASSERT_EQ(decode("+-)", 1), '-');
	ASSERT_EQ(decode("+-)", 2), ')');
}

TEST(RefEncoding, HasNext) {
	EXPECT_TRUE(has_next("foo", 0));
	EXPECT_TRUE(has_next("foo", 1));
	EXPECT_TRUE(has_next("++", 0));
	EXPECT_FALSE(has_next("++", 1));
	EXPECT_FALSE(has_next("+++", 2));
	EXPECT_FALSE(has_next("foo", 2));
}

TEST(RefEncoding, Concatenate) {
	EXPECT_TRUE(concat("", "foo"));
	EXPECT_TRUE(concat("", "+"));
	EXPECT_TRUE(concat("foo", "bar"));
	EXPECT_TRUE(concat("ok+", "foo"));
	EXPECT_TRUE(concat("a", "b"));
}
