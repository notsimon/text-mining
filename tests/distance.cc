#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstring>

#include <gtest/gtest.h>

#include <vector>

typedef unsigned int uint;

struct PackedNode {
	std::string label;
	uint freq;
	std::vector<PackedNode> childs;

	PackedNode (
			const std::string& label, uint freq,
			std::initializer_list<PackedNode> childs = {}
		)
		: label (label),
		  freq (freq),
		  childs (childs) {
	}
};

template <typename T>
T array_min (const T* array, size_t size) {
	T min = array[0];

	for (uint i = 1; i < size; i++)
		min = std::min(min, array[i]);

	return min;
}

uint levenshtein(const std::string& word, const std::string& label, int idx, uint* prev_row, uint* prev_2_row)
{
	const uint rowSize = word.size() + 1; // TODO global
    uint curRow [rowSize];

    for (uint i = idx + 1; i < label.size() + 1; i++) {
    	curRow[0] = prev_row[0] + 1;

	    for (uint j = 1; j < rowSize; j++) {
	        // Distance minimum pour la substitution et la transposition
	        uint cost = word[j - 1] == label[i - 1] ? 0 : 1;

	        uint del = prev_row[j] + 1; // Coût de la suppression d'un caractère
	        uint ins = curRow[j - 1] + 1; // Coût de l'insertion d'un caractère
	        uint subs = prev_row[j - 1] + cost; // Coût du remplacement d'un caractère

	        // Cout minimum
	        uint cmin = std::min(std::min(ins, del), subs);

	        // Coût de la transposition d'un caractère
	        if (j > 1 && i > 1
	         && word[j - 1] == label[i - 2]
	         && word[j - 2] == label[i - 1])
	        	cmin = std::min(cmin, prev_2_row[j - 2] + cost);

	        curRow[j] = cmin;
	    }

	    std::memcpy(prev_2_row, prev_row, rowSize * sizeof(uint));
	    std::memcpy(prev_row, curRow, rowSize * sizeof(uint));
	}

    return curRow[rowSize - 1];
}

/**
 * \return the number of matching words found
 */
uint levenshtein_rec (
			const std::string& word, const PackedNode& node, uint max_dist,
			std::string accu, const uint* prev_row, const uint* prev_2_row
		) {
	uint results = 0;
	const uint rowSize = word.size() + 1; // TODO global
	uint row_n_1[rowSize]; // n - 1
	uint row_n_2[rowSize]; // n - 2

	std::memcpy(row_n_1, prev_row, rowSize * sizeof(uint));
	std::memcpy(row_n_2, prev_2_row, rowSize * sizeof(uint));

	int index = accu.length();
	accu += node.label; // TODO optimize !

	uint dist = levenshtein(word, accu, index, row_n_1, row_n_2);

	//std::cout << '[' << node.label << "] dist = " << dist << std::endl;

	if (dist <= max_dist && node.freq > 0) {
		results++;
		// ajouter resultat à la liste
		std::cout << "   { word = " << accu;
		std::cout << ", freq = " << node.freq;
		std::cout << ", dist = " << dist << " }" << std::endl;
	}

	if (array_min(row_n_1, rowSize) <= max_dist) {
		for (const PackedNode& child : node.childs)
			results += levenshtein_rec(
					word, child, max_dist,
					accu, row_n_1, row_n_2
				);
	}

	return results;
}

/**
 * \return the number of matching words found
 */
uint approx (const std::string& word, const PackedNode& trie, uint max_dist) { // label must be removed and set to the node
	const uint rowSize = word.size() + 1;
	uint row_n_1[rowSize]; // n - 1
	uint row_n_2[rowSize]; // n - 2

	for (uint i = 0; i < word.size() + 1; i++) {
		row_n_1[i] = i;
		row_n_2[i] = i;
	}

	std::cout << '[' << word << ']' << std::endl;

	uint results = levenshtein_rec(word, trie, max_dist, "", row_n_1, row_n_2);

	return results;
}

// Levenshtein

uint simple_levenshtein (const std::string& word, const std::string& label) {
	uint prevRow[word.size() + 1];
	uint prev2Row[word.size() + 1];

	for (uint i = 0; i < word.size() + 1; i++) {
		prevRow[i] = i;
		prev2Row[i] = i;
	}

	return levenshtein(word, label, 0, prevRow, prev2Row);
}

TEST(Levenshtein, Equality) { // TODO only equalities
	ASSERT_EQ(simple_levenshtein("aab", "aa"), (uint)1);
	ASSERT_EQ(simple_levenshtein("aabc", "aa"), (uint)2);
	ASSERT_EQ(simple_levenshtein("bb", "aa"), (uint)2);
	ASSERT_EQ(simple_levenshtein("b", "a"), (uint)1);
}

// Patricia

PackedNode trie ("to", 0, {
					PackedNode("to", 3),
					PackedNode("ti", 2),
					PackedNode("tu", 2),
					PackedNode("fu", 0, {
						PackedNode("bar", 1),
						PackedNode("baz", 4)
					})
				});

TEST(PatriciaSearch, DistanceZero) {
	ASSERT_EQ(approx("toto", trie, 0), (uint)1);
	ASSERT_EQ(approx("toti", trie, 0), (uint)1);
	ASSERT_EQ(approx("tofu", trie, 0), (uint)0);
	ASSERT_EQ(approx("tofubar", trie, 0), (uint)1);
}

TEST(PatriciaSearch, DistanceOne) {
	ASSERT_EQ(approx("toto", trie, 1), (uint)3);
	ASSERT_EQ(approx("tofu", trie, 1), (uint)1);
	ASSERT_EQ(approx("tofubar", trie, 1), (uint)2);
	ASSERT_EQ(approx("tofubra", trie, 1), (uint)1);
	ASSERT_EQ(approx("tofbuar", trie, 1), (uint)1);
	ASSERT_EQ(approx("otto", trie, 1), (uint)1);
	ASSERT_EQ(approx("itoto", trie, 1), (uint)1);
	ASSERT_EQ(approx("tioto", trie, 1), (uint)1);
	ASSERT_EQ(approx("to5to", trie, 1), (uint)1);
	ASSERT_EQ(approx("t5oto", trie, 1), (uint)1);
	ASSERT_EQ(approx("tot6o", trie, 1), (uint)1);
}

TEST(PatriciaSearch, DistanceMore) {
	ASSERT_EQ(approx("toto", trie, 2), (uint)3);
	ASSERT_EQ(approx("toto", trie, 3), (uint)3);
}
