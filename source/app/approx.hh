#pragma once

#include <common.hh>
#include "result.hh"

class PackedTrie;
struct PackedNode;
class RefEncodedString;

/**
 * Accumulate the results of the successives calls to method approx
 */
class Approx {
	public:
		Approx (const PackedTrie& t)
			: trie(t)
			, rowSize(0)
		{ }

		const Result& approx (const std::string& word, uint max_dist);

		inline const Result& operator() (const std::string& word, uint max_dist) {
			return approx(word, max_dist);
		}

		void levenshtein_rec (
			const std::string& word, const PackedNode& node, uint max_dist,
			std::string accu, const uint* prev_row, const uint* prev_2_row
		);

		static uint levenshtein (
			const std::string& word, const std::string& label, int idx,
			uint* prev_row, uint* prev_2_row
		);
	private:
		Result result_;
		const PackedTrie& trie;
		unsigned int rowSize;
};
