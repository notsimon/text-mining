#pragma once
#include "result.hh"

inline std::ostream& operator<<(std::ostream& out, output_format format)
{
	switch (format) {
		case output_format::HUMAN:
			out.iword(Result::output_format_index) = 1;
			break;
		case output_format::JSON:
			out.iword(Result::output_format_index) = 0;
			break;
	}

	return out;
}

inline std::ostream& operator<<(std::ostream& out, const Result& res)
{
	if (out.iword(Result::output_format_index) == 0)
		res.output_json(out);
	else if (out.iword(Result::output_format_index) == 1)
		res.output_json(out);
	else
		// WTF? Impossible values
		assert(Result::output_format_index != 0 && Result::output_format_index != 1);

	return out;
}

inline void Result::clear() {
	res_.erase(res_.begin(), res_.end());
}
