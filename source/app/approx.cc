#include "approx.hh"
#include <common/packed_trie.hh>
#include <common/ref-encoded-string.hh>

namespace {
	template <typename T>
	inline T array_min (const T* array, size_t size) {
		T min = array[0];

		for (uint i = 1; i < size; i++)
			min = std::min(min, array[i]);

		return min;
	}
}

/**
 * \return the number of matching words found
 */
const Result& Approx::approx (const std::string& word, uint max_dist) {
	result_.clear();
	rowSize = word.size() + 1;

	uint row_n_1[rowSize]; // n - 1
	uint row_n_2[rowSize]; // n - 2

	for (uint i = 0; i < word.size() + 1; i++) {
		row_n_1[i] = i;
		row_n_2[i] = i;
	}

	for (const PackedNode& root : trie.roots())
		levenshtein_rec(word, root, max_dist, "", row_n_1, row_n_2);

	return result_;
}

/**
 * \return the number of matching words found
 */
void Approx::levenshtein_rec (
			const std::string& word, const PackedNode& node, uint max_dist,
			std::string accu, const uint* prev_row, const uint* prev_2_row
		) {
	uint row_n_1[rowSize]; // n - 1
	uint row_n_2[rowSize]; // n - 2

	std::memcpy(row_n_1, prev_row, rowSize * sizeof(uint));
	std::memcpy(row_n_2, prev_2_row, rowSize * sizeof(uint));

	int index = accu.length();
	accu += node.label; // TODO optimize this !

	uint dist = levenshtein(word, accu, index, row_n_1, row_n_2);

	if (dist <= max_dist && node.freq > 0)
		result_.add(accu, node.freq, dist);

	// stop if there is no chance to find another matches
	if (array_min(row_n_1, rowSize) <= max_dist) {
		for (const PackedNode& child : node)
			levenshtein_rec(word, child, max_dist, accu, row_n_1, row_n_2);
	}
}

uint Approx::levenshtein (const std::string& word, const std::string& label,
						  int idx, uint* prev_row, uint* prev_2_row)
{
	const uint rowSize = word.size() + 1;
    uint curRow [rowSize];

    for (uint i = idx + 1; i < label.size() + 1; i++) {
    	curRow[0] = prev_row[0] + 1;

	    for (uint j = 1; j < rowSize; j++) {
	        // Distance minimum pour la substitution et la transposition
	        uint cost = word[j - 1] == label[i - 1] ? 0 : 1;

	        uint del = prev_row[j] + 1; // Coût de la suppression d'un caractère
	        uint ins = curRow[j - 1] + 1; // Coût de l'insertion d'un caractère
	        uint subs = prev_row[j - 1] + cost; // Coût du remplacement d'un caractère

	        // Cout minimum
	        uint cmin = std::min(std::min(ins, del), subs);

	        // Coût de la transposition d'un caractère
	        if (j > 1 && i > 1 && word[j - 1] == label[i - 2]
				&& word[j - 2] == label[i - 1])
	            cmin = std::min(cmin, prev_2_row[j - 2] + cost);

	        curRow[j] = cmin;
	    }

	    std::memcpy(prev_2_row, prev_row, rowSize * sizeof(uint));
	    std::memcpy(prev_row, curRow, rowSize * sizeof(uint));
	}

    return curRow[rowSize - 1];
}

