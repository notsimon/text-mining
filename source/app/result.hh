#pragma once

# include <cassert>
# include <climits>
# include <cmath>
# include <iostream>
# include <map>
# include <string>

/**
* Class for holding requests results
*/
class Result {
	public:
		// Multimap comparator
		struct MMComp {
			bool operator() (unsigned int lhs, unsigned int rhs) const
			{
				return lhs > rhs;
			}
		};

		// Typedefs
		typedef std::map<unsigned int,
			std::multimap<unsigned int, std::string, MMComp>> ResMap;
		typedef std::multimap<unsigned int, std::string, MMComp> ResMMap;
		typedef std::pair<unsigned int,	ResMMap> ResElem;
		typedef std::pair<unsigned int, std::string> ResDElem;

		// Ctor & Dtor
		Result();
		Result(const Result& res);
		~Result();

		// Member functions
		void clear();

		inline void add(const std::string& w, unsigned int f, unsigned int d) {
			res_[d].insert(ResDElem(f, w));
		}

		// Toolbox
		std::ostream& output_human(std::ostream& out) const;
		std::ostream& output_json(std::ostream& out) const;

		static const int output_format_index;

	private:
		ResMap res_;
};

/**
* Modifiers
*/
enum output_format {
	HUMAN,
	JSON
};

std::ostream& operator<<(std::ostream& out, output_format format);

/**
* Helpers to print to the desired format
*/
std::ostream& operator<<(std::ostream& out, const Result& res);

#include "result.hxx"
