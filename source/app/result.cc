#include "result.hh"

const int Result::output_format_index = std::ios_base::xalloc();

Result::Result() {
	std::cout.iword(output_format_index) = 0;
}

Result::Result(const Result& res) {
	std::cout.iword(output_format_index) = 0;

	for (ResElem e: res.res_)
		for (ResDElem f: e.second)
			add(f.second, f.first, e.first);
}

Result::~Result() {
}

std::ostream& Result::output_json(std::ostream& out) const
{
	out << "[";

	unsigned int j, i = 1;
	ResMMap::const_iterator iit;
	for (ResMap::const_iterator it = res_.begin();
		 it != res_.end();
		 it++, ++i)
		for (j = 1, iit = it->second.begin();
			 iit != it->second.end();
			 iit++, ++j) {
			out << "{";
			out << "\"word\":\"" << iit->second << "\",";
			out << "\"freq\":" << iit->first << ",";
			out << "\"distance\":" << it->first;
			out << "}";

			if (i != res_.size() || j != it->second.size())
				out << ",";
		}

	out << "]";

	return out;
}

