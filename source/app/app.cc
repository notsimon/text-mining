#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <climits>
#include <queue>

#include <common/packed_dump.hh>
#include <common/packed_trie.hh>
#include <common/packed_node.hh>
#include <app/approx.hh>

uint depth_traversal (const PackedNode& node) {
	uint i = 1;

	std::cout << node.num << std::endl;

	for (uint k = 0; k < node.num; k++) {
		const PackedNode* child = node.child(k);
		assert(child > &node);
		i += depth_traversal(*child);
	}

	return i;
}

uint index_traversal (PackedTrie& trie) {
	int i = 0;

	for (auto it = trie.begin(); it != trie.end(); ++it) {
		std::cout << (*it).num << std::endl;
		i++;
	}

	return i;
}

int main (int argc, char* argv[]) {
	if (argc != 2) {
		std::cout << "Usage: " << argv[0]
				  << " /path/to/output/dict.bin"
				  << std::endl;

		return 1;
	}

	// READING

	const PackedTrie* trie = packed_load(std::string(argv[1]));

	//depth_traversal(*trie->sentinel);
	//index_traversal(*trie);

	// APP

	Approx searcher(*trie);

	std::string line;
	while (!std::cin.eof()) {
		std::getline(std::cin, line);
		std::stringstream ss(line);
		std::string cmd;
		std::string wd;
		int dist = INT_MAX;

		ss >> cmd >> dist >> wd;

		if (cmd.compare("approx") == 0 && !wd.empty() && dist != INT_MAX) {
			std::cout << searcher.approx(wd, dist) << std::endl;
		}
	}

	delete trie;

	return 0;
}
