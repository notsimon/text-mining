#pragma once
#include "packed_trie.hh"

inline PackedTrie::PackedTrie (char* raw, size_t size)
	: rawbegin_ (raw),
	  rawend_ (raw + size) {
}

inline PackedTrie::~PackedTrie () {
	delete[] rawbegin_;
}

inline PackedNode& PackedTrie::roots () {
	return *begin_;
}

inline const PackedNode& PackedTrie::roots () const {
	return *begin_;
}

inline PackedTrie::iterator PackedTrie::begin () {
	return iterator(begin_);
}

inline PackedTrie::const_iterator PackedTrie::begin () const {
	return const_iterator(begin_);
}

inline PackedTrie::iterator PackedTrie::end () {
	return iterator(end_);
}

inline PackedTrie::const_iterator PackedTrie::end () const {
	return const_iterator(end_);
}

// Iterator

template <typename ValueType>
PackedTrie::Iterator<ValueType>::Iterator (ValueType* p)
	: p_ (p) {
}

template <typename ValueType>
ValueType& PackedTrie::Iterator<ValueType>::operator* () {
	return *p_;
}

/**
 * Increment operator
 */
template <typename ValueType>
PackedTrie::Iterator<ValueType>& PackedTrie::Iterator<ValueType>::operator++ () {
	p_ = reinterpret_cast<ValueType*>(p_->offset + p_->num);
	return *this;
}

template <typename ValueType>
PackedTrie::Iterator<ValueType>& PackedTrie::Iterator<ValueType>::operator++ (int) {
	return ++(*this);
}

/**
 * Equality operator
 */
template <typename ValueType>
bool PackedTrie::Iterator<ValueType>::operator== (const PackedTrie::Iterator<ValueType>& rhs) {
	return p_ == rhs.p_;
}

/**
 * Difference operator
 */
template <typename ValueType>
bool PackedTrie::Iterator<ValueType>::operator!= (const PackedTrie::Iterator<ValueType>& rhs) {
	return p_ != rhs.p_;
}