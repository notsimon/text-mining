#pragma once

#include <fstream>
#include <string>

// Forwards

class PatriciaTrie;
class PackedTrie;

/**
 * Dump a \struct PatriciaTrie to a file in 'packed' form
 * which can be loaded as a \struct PackedTrie
 */
void packed_save (const std::string&, const PatriciaTrie&);

/**
 * Load a 'packed' patricia trie from file in a \struct PackedTrie
 */
const PackedTrie* packed_load (const std::string&);