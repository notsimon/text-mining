#pragma once
#include "packed_node.hh"
#include <cassert>
#include <iostream>

inline PackedNode::PackedNode ()
	: label (),
	  freq (0),
	  num (0) {
}

inline PackedNode::PackedNode (const uchar* label, uint freq, uint num)
	: label (label),
	  freq (freq),
	  num (num) {
}

inline PackedNode* PackedNode::child (uint i) {
	assert(i < num);
	assert(offset[i] != 0);
	return reinterpret_cast<PackedNode*>(
				reinterpret_cast<char*>(this)
				+ offset[i]
			);
}

inline const PackedNode* PackedNode::child (uint i) const {
	assert(i < num);
	assert(offset[i] != 0);
	return reinterpret_cast<const PackedNode*>(
				reinterpret_cast<const char*>(this)
				+ offset[i]
			);
}

inline PackedNode::iterator PackedNode::begin () {
	return iterator(*this, 0);
}

inline PackedNode::const_iterator PackedNode::begin () const {
	return const_iterator(*this, 0);
}

inline PackedNode::iterator PackedNode::end () {
	return iterator(*this, num);
}

inline PackedNode::const_iterator PackedNode::end () const {
	return const_iterator(*this, num);
}

// Iterator

template <typename ValueType>
inline PackedNode::Iterator<ValueType>::Iterator (ValueType& node, uint i)
	: node_ (node),
	  i_ (i) {
}

template <typename ValueType>
inline ValueType& PackedNode::Iterator<ValueType>::operator* () {
	assert(i_ < node_.num);
	return *node_.child(i_);
}

/**
 * Increment operator
 */
template <typename ValueType>
inline PackedNode::Iterator<ValueType>& PackedNode::Iterator<ValueType>::operator++ () {
	i_++;
	return *this;
}

template <typename ValueType>
inline PackedNode::Iterator<ValueType>& PackedNode::Iterator<ValueType>::operator++ (int) {
	return ++(*this);
}

/**
 * Equality operator
 */
template <typename ValueType>
inline bool PackedNode::Iterator<ValueType>::operator== (const PackedNode::Iterator<ValueType>& rhs) {
	return &node_ == &rhs.node_ && i_ == rhs.i_;
}

/**
 * Difference operator
 */
template <typename ValueType>
inline bool PackedNode::Iterator<ValueType>::operator!= (const PackedNode::Iterator<ValueType>& rhs) {
	return i_ != rhs.i_ || &node_ != &rhs.node_;
}