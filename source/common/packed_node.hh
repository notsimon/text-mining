#pragma once

#include <common.hh>
#include "ref-encoded-string.hh"

#include <cstdint>
#include <iterator>
#include <cstring>

/**
 * PackedNode
 */

struct PackedNode {
	// types
	template <typename ValueType>
	class Iterator;

	typedef uint offset_t;

	// public members
	RefEncodedString label;
	uint freq;
	ushort num;
	offset_t offset[0]; // allow access to the end of the struct

	/*union { // INFO: removed for cache optimization
		offset_t offset[0]; // offset_t = uintptr_t
		PackedNode* child[0];
	} __attribute__((packed)); */

public:
	// public types
	typedef Iterator<PackedNode> iterator;
	typedef Iterator<const PackedNode> const_iterator;

	PackedNode ();

	PackedNode (const uchar* inlabel, uint freq, uint num);

	PackedNode* child (uint i);
	const PackedNode* child (uint i) const;

	/**
	 * Begin at the first root, thus ignore the sentinel
	 */
	iterator begin ();
	const_iterator begin () const;
	
	iterator end ();
	const_iterator end() const;
} __attribute__((packed));


/**
 * PackedNode childs iterator
 */

template <typename ValueType>
class PackedNode::Iterator
	: std::iterator<std::forward_iterator_tag, ValueType> {

public:
	Iterator (ValueType&, uint);

	ValueType& operator* ();
	Iterator<ValueType>& operator++ ();
	Iterator<ValueType>& operator++ (int);

	bool operator== (const Iterator<ValueType>& rhs);
	bool operator!= (const Iterator<ValueType>& rhs);

private:
	ValueType& node_;
	uint i_;
};

#include "packed_node.hxx"
