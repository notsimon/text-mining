#include "packed_dump.hh"

#include <common.hh>
#include <common/packed_trie.hh>
#include <common/packed_node.hh>

#include <compiler/patricia-trie.hh>
#include <compiler/patricia-node.hh>

#include <boost/tuple/tuple.hpp>
#include <queue>

namespace {
	std::streampos dump (std::ofstream& out, const PackedNode& node) {
		std::streampos strpos = out.tellp();

		out.write(reinterpret_cast<const char*>(&node), sizeof(PackedNode)); // dump the node
		out.seekp( // add blanks for childs offsets
				node.num * sizeof(PackedNode::offset_t),
				std::ios_base::cur
			);

		return strpos;
	}

	void recursive_dump (
				std::ofstream& out,
				const PatriciaNode& patricia_node,
				std::streampos parent_pos,
				std::streampos offset_pos
			) {
		// dump PatriciaNode attributes and offsets blank spaces
		PackedNode packed_node (
						patricia_node.label(),
						patricia_node.freq(),
						patricia_node.children().size()
					);

		std::streampos node_pos = dump(out, packed_node);

		// compute the offset relative to the parent position
		PackedNode::offset_t offset = node_pos - parent_pos;
		assert(offset != 0);

		// update the offset field in the parent node
		std::streampos back = out.tellp(); // end of the current node in the file
		out.seekp(offset_pos);
		out.write((char*)&offset, sizeof(offset));
		out.seekp(back); // return to the end of the dumped node

		// dump node's children
		offset_pos = node_pos + (std::streampos)(sizeof(PackedNode)); // first offset field at end of the PackedNode struct
		for (const PatriciaNode* child : patricia_node.children()) {
			recursive_dump(out, *child, node_pos, offset_pos);
			offset_pos += (std::streampos)(sizeof(PackedNode::offset_t));
		}
	}
}

void packed_save (const std::string& filepath, const PatriciaTrie& trie) {
	std::ofstream out;
	out.open(filepath, std::ios::binary);

	// sentinel
	PackedNode sentinel;
	sentinel.num = (uint)trie.roots().size();

	std::streampos node_pos = dump(out, sentinel);

	// enqueue roots
	std::streampos offset_pos = node_pos
							  + static_cast<std::streampos>(sizeof(PackedNode));

	for (const PatriciaNode* root : trie.roots()) {
		recursive_dump(out, *root, node_pos, offset_pos);
		offset_pos += (std::streampos)(sizeof(PackedNode::offset_t));
	}

	out.close();
}

/*inline PackedNode* offset_to_ptr (PackedNode& node, uint i) {
	assert(i < node.num);
	assert(node.offset[i] != 0);
	return reinterpret_cast<PackedNode*>(
				reinterpret_cast<char*>(&node)
				+ node.offset[i]
			);
}*/

const PackedTrie* packed_load (const std::string& filepath) {
	// file binary loading

	std::ifstream in;
	in.open(filepath, std::ios::binary);

	in.seekg(0, std::ios_base::end);
	std::streamsize blob_size = in.tellg();
	in.seekg(0, std::ios_base::beg);

	char* blob =  new char[blob_size];
	in.read(blob, blob_size);

	in.close();

	// patricia trie
	PackedTrie* trie = new PackedTrie(blob, blob_size);

	// reconstruct pointers
	// INFO: removed for cache optimization
	/*for (auto& node : *trie) {
		for (uint k = 0; k < node.num; k++)
			node.child[k] = offset_to_ptr(node, k);
	}*/

	return trie;
}
