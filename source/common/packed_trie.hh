#pragma once

#include "packed_node.hh"
#include <iostream>
#include <iterator>

/**
 * PackedTrie
 */
class PackedTrie {
	// types
	template <typename ValueType>
	class Iterator;

private:
	const union {
		PackedNode* begin_;
		char* rawbegin_;
	};

	const union {
		PackedNode* end_;
		char* rawend_;
	};

public:
	typedef Iterator<PackedNode> iterator;
	typedef Iterator<const PackedNode> const_iterator;

	/**
	 * The PackedTrie owns the data and thus will free it
	 * on destruction
	 */
	PackedTrie (char* raw, size_t size);

	~PackedTrie ();

	/**
	 * Begin at the sentinel, which has all roots for childs
	 */
	iterator begin ();
	const_iterator begin () const;

	/**
	 * The end of the entire memory dump
	 */
	iterator end ();
	const_iterator end () const;

	/**
	 * Return the sentinel, which can be used to iterate
	 * over the trie's roots
	 */
	PackedNode& roots ();
	const PackedNode& roots () const;
};


/**
 * PackedTrie iterator
 */
template <typename ValueType>
class PackedTrie::Iterator
	: std::iterator<std::forward_iterator_tag, ValueType> {

public:
	Iterator (ValueType* p);

	ValueType& operator* ();
	Iterator<ValueType>& operator++ ();
	Iterator<ValueType>& operator++ (int);

	bool operator== (const Iterator<ValueType>& rhs);
	bool operator!= (const Iterator<ValueType>& rhs);

private:
	ValueType* p_;
};

#include "packed_trie.hxx"