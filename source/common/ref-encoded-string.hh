#pragma once

#include <string>
#include <cassert>
#include <cstring>
#include "common.hh"

#include <iterator>

/**
* Ref encoding for strings
*/
class RefEncodedString {
public:	
	/**
	* Ctors
	*/
	RefEncodedString ()
		: str {0} {
	}

	RefEncodedString(const std::string& s)
	{
		uint k;
		for (k = 0; k < s.length() - 1 && k < LABEL_SIZE - 1; ++k)
			str[k] = static_cast<uchar>(s[k] | 0x80);
		str[k] = s[k];
	}

	RefEncodedString(const uchar* cstr)
	{
		std::memcpy(str, cstr, LABEL_SIZE);
	}

	inline bool has_next(int j) const {
		assert(j >= 0 && j < LABEL_SIZE);
		return str[j] & 0x80;
	}

	/**
	* Decoding method
	*/
	inline char dec(int j) const {
		assert(j >= 0 && j < LABEL_SIZE);
		return str[j] & 0x7F;
	}

	inline char operator[] (uint i) const {
		return str[i];
	}

private:
	uchar str[LABEL_SIZE];
};	

/**
* Concatenate a ref encoded string to a string
*/
inline std::string& operator+=(std::string& str, const RefEncodedString& res) {
	int i;
	for (i = 0; res.has_next(i); i++)
		str += res.dec(i);
	str += res.dec(i);

	return str;
}
