#pragma once
#ifndef PATRICIA_TRIE_HXX
# define PATRICIA_TRIE_HXX

inline PatriciaTrie::iterator PatriciaTrie::begin()
{
	return roots_.begin();
}

inline PatriciaTrie::iterator PatriciaTrie::end()
{
	return roots_.end();
}

inline PatriciaNode::node_list& PatriciaTrie::roots()
{
	return roots_;
}

inline const PatriciaNode::node_list& PatriciaTrie::roots() const
{
	return roots_;
}

#endif /* !PATRICIA_TRIE_HXX */
