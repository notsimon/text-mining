#pragma once
#ifndef PATRICIA_DEBUG_HH
# define PATRICIA_DEBUG_HH

# include <iostream>
# include "patricia-trie.hh"

/**
* Pretty prints the given trie
*/
void print(PatriciaTrie& trie);
void prettyprint(std::string pref, const PatriciaNode& node);

#endif // !PATRICIA_DEBUG_HH
