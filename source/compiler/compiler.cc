#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "common.hh"
#include "common/packed_dump.hh"
#include "compiler/patricia-trie.hh"
#include "compiler/patricia-debug.hh"

int main (int argc, char* argv[]) {
	if (argc <= 1) {
		std::cout << "Usage: ./" << argv[0]
				  << " /path/to/word/freq.txt /path/to/output/dict.bin" << std::endl;
		return 1;
	}

	// READING
	std::ifstream in;
	in.open(argv[1]);

	PatriciaTrie trie;
	
	std::string word;
	uint freq;
	while (!in.eof()) {
		in >> word;
		in >> freq;

		trie.insert(word, freq);
	}

	in.close();

	// WRITING
	//auto start_time = std::clock();

	packed_save(std::string(argv[2]), trie);

	//auto ellapsed_time = std::clock() - start_time;
	//std::cout << (ellapsed_time / double(CLOCKS_PER_SEC)) << std::endl;

	return 0;
}
