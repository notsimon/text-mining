#pragma once
#ifndef PATRICIA_NODE_HH
# define PATRICIA_NODE_HH

# include <common.hh>

# include <cassert>
# include <vector>
# include <string>
/**
* PatriciaNode class represents a node in a patricia trie. It holds a label,
* a frequency and its children
*/
class PatriciaNode
{
	public:
		// Typedefs
		/**
		* Type of children list
		*/
		typedef std::vector<PatriciaNode*> node_list;

		/**
		* Type of children iterator
		*/
		typedef node_list::iterator iterator;

		/**
		* Type of children const_iterator
		*/
		typedef node_list::const_iterator const_iterator;

		// Ctor & Dtor

		/**
		* Constructor for a new node 
		*/
		PatriciaNode(unsigned char* label, unsigned int freq);

		/**
		* Constructor for a node with defined children (split case)
		*/
		PatriciaNode(unsigned char* label, unsigned int freq, node_list& children);

		/**
		* Constructor for a not-normalized string
		*/
		PatriciaNode(const std::string& label, unsigned int freq);

		/**
		* Destructor, delete children in the vector
		*/
		~PatriciaNode();

		// Trie toolbox

		/**
		* Insert a word from this node
		*/
		void insert(const std::string& word, unsigned int freq);

		// Iterators

		/**
		* Children beginning iterator
		*/
		iterator begin();

		/**
		* Children ending iterator
		*/
		iterator end();


		/**
		* Children beginning const_iterator
		*/
		const_iterator begin() const;

		/**
		* Children ending const_iterator
		*/
		const_iterator end() const;

		// Getters & Setters

		/**
		* Label getter
		*/
		unsigned char* label();

		/**
		* Label getter const
		*/
		const unsigned char* label() const;

		/**
		* Frequency getter
		*/
		unsigned int freq() const;

		/**
		* Children getter
		*/
		node_list& children();

		/**
		* Children getter const
		*/
		const node_list& children() const;

	private:
		// Trie toolbox
		void split(unsigned int pos);

		// Attributes
		unsigned char label_[LABEL_SIZE];
		unsigned int freq_;
		node_list children_;
};

# include "patricia-node.hxx"

/**
* Selects the trie in list where word can fit
* Returns null pointer if no one corresponds to the word
*/
template <typename T>
PatriciaNode::iterator select_trie(std::vector<PatriciaNode>& list,
								   T& word);

#endif // !PATRICIA_NODE_HH
