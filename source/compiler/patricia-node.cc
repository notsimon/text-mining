#include "patricia-node.hh"
#include <cstring>

//Ctor & Dtor
PatriciaNode::PatriciaNode(unsigned char* label, unsigned int freq)
	: freq_(freq)
{
	memcpy(label_, label, LABEL_SIZE);
/*
	int i = 0;
	for (i = 0; i < LABEL_SIZE && label[i] & 0x80; ++i)
		label_[i] = label[i];

	label_[i] = label[i];

	for (int j = i + 1; j < LABEL_SIZE; ++j)
		label_[j] = 0;
*/
}

PatriciaNode::PatriciaNode(unsigned char* label,
		unsigned int freq,
		PatriciaNode::node_list& children)
	: PatriciaNode::PatriciaNode(label, freq)
{
	children_ = children;
}

PatriciaNode::PatriciaNode(const std::string& label, unsigned int freq)
	: freq_(0)
{
	unsigned int i = 0;
	for (i = 0; i < (LABEL_SIZE - 1) && i < (label.length() - 1); ++i)
		label_[i] = label[i] | 0x80;

	label_[i] = label[i];

	for (int j = i + 1; j < LABEL_SIZE; ++j)
		label_[j] = 0;

	if (label.length() > LABEL_SIZE) {
		PatriciaNode* lv1 = this;
		PatriciaNode* lv2;
		std::string word = label.substr(LABEL_SIZE);
		do {
			int size = word.length() > LABEL_SIZE ? LABEL_SIZE : word.length();
			lv2 = new PatriciaNode(word.substr(0, size), 0);
			lv1->children_.push_back(lv2);
			lv1 = lv2;
			word = word.substr(size);
		}
		while (word.length() > 0);
		lv2->freq_ = freq;
	}
	else
		freq_ = freq;
}

PatriciaNode::~PatriciaNode()
{
	for (PatriciaNode* n: children_)
		delete n;
}

#include <iostream>

void PatriciaNode::insert(const std::string& word, unsigned int freq) {
// We are in the correct node, lets insert the word into it
	unsigned int len = 0;
	bool prev = true;
	for (len = 0; len < LABEL_SIZE && (prev || label_[len] & 0x80)
		 && len < word.length() && (label_[len] & 0x7F) == word[len];
		 ++len)
	{
		prev = 0x80 & label_[len];
		// Getting max common length
	}

	if (len == word.length() && word[len - 1] ==  label_[len - 1])
	{
		// The word already exists
		// Stategy to be defined, right now: nothing to do
		if (freq_ == 0) freq_ = freq;
	}

	else if (len <= word.length() && !(label_[len - 1] & 0x80)) {
		// exactly the same prefix, browse children
		std::string new_word = word.substr(len, word.length() - len);
		iterator branch = select_trie(children_, new_word);
		if (branch != children_.end())
			return (*branch)->insert(new_word, freq);

		// No corresponding child found, create a node
		PatriciaNode *node = new PatriciaNode(new_word, freq);
		// Adding according to alphabetical order
		iterator it = begin();
		for (it = begin(); it != end(); it++)
			if (((*it)->label()[0] & 0x7F) > new_word[0]) {
				children_.insert(it, node);
				return;
			}
		children_.insert(it, node);
	} else {
		// Split the prefix, split the node and add a child
		split(len);
		return insert(word, freq);
	}
}

void PatriciaNode::split(unsigned int pos) {
	assert(pos > 0);

	PatriciaNode *node = new PatriciaNode(&label_[pos], freq_, children_);
	label_[pos - 1] &= 0x7F;
	for (int i = pos; i < LABEL_SIZE; ++i)
		label_[i] = 0;
	freq_ = 0;
	children_ = node_list(1, node);
}
