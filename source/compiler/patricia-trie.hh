#pragma once
#ifndef PATRICIA_TRIE_HH
# define PATRICIA_TRIE_HH

# include <string>
# include <vector>
# include "patricia-node.hh"

/**
* PatriciaTrie class stands for representing the trie :
* It can hold several roots, which will be represented with PatriciaNode class
*/
class PatriciaTrie
{
	public:
		// Typedefs
		/**
		* Type for representing roots
		*/
		typedef PatriciaNode::node_list node_list;

		/**
		* Iterator for iterating over roots
		*/
		typedef PatriciaNode::iterator iterator;

		// Ctor & Dtor
		/**
		* Default constructor, nothing special to do
		*/
		PatriciaTrie() = default;

		/**
		* Destructor
		* Free the memory allocated for roots
		*/
		~PatriciaTrie();

		// Iterators
		/**
		* Beginning iterator for roots
		*/
		iterator begin();

		/**
		* Ending iterator for roots
		*/
		iterator end();

		// Getters
		/**
		* Returns roots
		*/
		PatriciaNode::node_list& roots();

		/**
		* Return roots (const)
		*/
		const PatriciaNode::node_list& roots() const;

		/**
		* Insert a word in the trie
		*/
		void insert(const std::string& word, unsigned int freq);

	private:
		PatriciaNode::node_list roots_;
};

# include "patricia-trie.hxx"

#endif /* !PATRICIA_TRIE_HH */
