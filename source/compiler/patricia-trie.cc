#include "patricia-trie.hh"

PatriciaTrie::~PatriciaTrie() {
	for (PatriciaNode* n: roots_)
		delete n;
}

void PatriciaTrie::insert(const std::string& word, unsigned int freq) {
	iterator branch = select_trie(roots(), word);
	if (branch != roots().end())
		return (*branch)->insert(word, freq);

	// No node found, create one to add the word
	PatriciaNode* node = new PatriciaNode(word, freq);

	// Adding according to alphabetical order
	iterator it = begin();
	for (it = begin(); it != end(); it++)
		if (((*it)->label()[0] & 0x7F) > word[0]) {
			roots_.insert(it, node);
			return;
		}

	roots_.insert(it, node);
}
