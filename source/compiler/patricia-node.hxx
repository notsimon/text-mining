#pragma once
#ifndef PATRICIA_NODE_HXX
# define PATRICIA_NODE_HXX

// Getters
inline unsigned char* PatriciaNode::label()
{
	return label_;
}

inline const unsigned char* PatriciaNode::label() const
{
	return label_;
}

inline unsigned int PatriciaNode::freq() const
{
	return freq_;
}

inline PatriciaNode::node_list& PatriciaNode::children()
{
	return children_;
}

inline const PatriciaNode::node_list& PatriciaNode::children() const
{
	return children_;
}

inline PatriciaNode::iterator PatriciaNode::begin()
{
	return children_.begin();
}

inline PatriciaNode::iterator PatriciaNode::end()
{
	return children_.end();
}

template <typename T>
PatriciaNode::iterator select_trie(PatriciaNode::node_list& list,
								   T& word)
{
	for (PatriciaNode::iterator it = list.begin(); it != list.end(); it++)
		// Select the right child
		if (((*it)->label()[0] & 0x7F) == (word[0] & 0x7F))
			return it;

	return list.end();
}

inline PatriciaNode::const_iterator PatriciaNode::begin() const
{
	return children_.begin();
}

inline PatriciaNode::const_iterator PatriciaNode::end() const
{
	return children_.end();
}

#endif // !PATRICIA_NODE_HXX
