#include "patricia-debug.hh"

static int nb_nodes = 0;

void prettyprint(std::string pref, const PatriciaNode& node)
{
	nb_nodes++;
	std::string y;
	int i = 0;
	for (i = 0; i < LABEL_SIZE && (node.label()[i] & 0x80); ++i)
		y += (char)(node.label()[i] & 0x7F);
	y += node.label()[i];
	std::string s = pref + y;
	if (node.freq() > 0)
		std::cout << "\t[WORD] \"complete: " << s << " - node: " << y
				  << "\"; freq: " << node.freq() << std::endl;
	else
		std::cout << std::endl << "\tStarting with: \""
				  << s << "\"" << std::endl;

	for (PatriciaNode* n: node.children())
		prettyprint(s, *n);
}

void print(PatriciaTrie& trie) {
	for (PatriciaNode* n: trie.roots()) {
		std::cout << "[ROOT] ";
		prettyprint("", *n);
		std::cout << std::endl;
	}

	std::cout << "[NB NODES] " << nb_nodes << std::endl;
}
