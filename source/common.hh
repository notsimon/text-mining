#pragma once

// TYPES

typedef unsigned int uint;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef unsigned short ushort;
typedef unsigned char uchar;

// DEFINES

#define LABEL_SIZE 4